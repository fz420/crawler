package controllers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	_ "github.com/mattn/go-sqlite3"
)

type HouseModelController struct {
	beego.Controller
}

func init() {
	orm.RegisterDriver("sqlite3", orm.DRSqlite)
	orm.RegisterDataBase("default", "sqlite3", "house.db")
}

func (c *HouseModelController) Get() {
	// c.ctx.
	// c.TplName = "index.tpl"

}
