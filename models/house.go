// house
package main

import (
	"github.com/astaxie/beego/orm"
)

type House struct {
	Id int
}

func init() {
	orm.RegisterModel(new(House))
}
